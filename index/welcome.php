<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TRU  CREATIVE</title>
    
    <!-- animate -->
    <link rel="stylesheet" href="../assets/css/animate.css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="../assets/css/magnific-popup.css">
    <!-- owl carousel -->
    <link rel="stylesheet" href="../assets/css/owl.carousel.min.css">
    <!-- icons -->
    <link rel="stylesheet" href="../assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/css/line-awesome.min.css">
    <link rel="stylesheet" href="../assets/css/themify-icons.css">
    <link rel="stylesheet" href="../assets/css/flaticon.css">
    <!-- slick slider -->
    <link rel="stylesheet" href="../assets/css/slick.css">
    <!-- animated slider -->
    <link rel="stylesheet" href="../assets/css/animated-slider.css">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="../assets/css/style.css">
    <!-- responsive Stylesheet -->
    <link rel="stylesheet" href="../assets/css/responsive.css">

</head>
<body>

<!-- preloader area start -->
<div class="preloader" id="preloader">
    <div class="preloader-inner">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<!-- preloader area end -->


<!-- navbar area start -->
<nav class="navbar navbar-area navbar-expand-lg nav-style-01">
    <div class="container nav-container">
        <div class="responsive-mobile-menu">
            <div class="logo-wrapper mobile-logo">
                <a href="welcome.html" class="logo">
                   <img src="../assets/img/logo.png" alt="logo">
                </a>
            </div>
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#Riyaqas_main_menu" 
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggle-icon">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="Riyaqas_main_menu">
            <div class="logo-wrapper desktop-logo">
                <a href="welcome.html" class="logo">
                    <img src="../assets/img/logo.png" alt="logo">
                </a>
            </div>
            <ul class="navbar-nav">
                <li >
                    
                    <a href="welcome.html">Accueil</a>
                
                </li>
                
                <li>
                    <a href="welcome/formation.html">Formations</a>
                </li>
                <li>
                     <a href="welcome/realisation.html">Realisations</a>
                    
                </li>
                <li>
                    <a href="team.html">Team</a>
               </li> 
                <li>
                    <a href="welcome/contact.html">Contact</a>
                </li>
            </ul>
        </div>
        <div class="nav-right-content">
            <a href="welcome/devis.html" class="btn btn-radius btn-red mr-2 mb-2">Demander un devis de site web</a>
        </div>
    </div>
</nav>
<!-- navbar area end --><!-- header area start -->
<div class="header-area header-bg pt-5" style="background-image:url(../../localhost/assets/img/bg/banner-bg.html);">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-6 col-md-6 offset-xl-1">
                <div class="header-inner-details">
                    <div class="header-inner">
                        <h1 class="title s-animate-1">AGENCE <span> DIGITALE</span> ET DE <span>FORMATION</span> EN DEVELOPPEMENT INFORMATIQUE</h1>
                        <p class="s-animate-2">Expert développeur Web,Mobile,Chatbots et ERP,référenceur et infographiste:une <br> polyvalence d'exception!</p>
                        <div class="btn-wrapper desktop-left padding-top-20 wow animated fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                            <a href="welcome/devis.html" class="btn btn-radius btn-red-border mr-2 mb-2">Demander un devis de site web</a>
                            <a href="welcome/formation.html" class="btn btn-radius btn-red mb-2">Choisir une formation</a>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="col-lg-6 col-md-6 hidden-sm">
                <img class="header-inner-img" src="../assets/img/banner/8.png" alt="banner-img">
            </div>
        </div>
    </div>
</div>
<!-- header area End -->

<!-- service area start -->
<div class="service-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-10">
                <div class="section-title text-center margin-bottom-90">
                    <h2 class="title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">Quel <span>Service</span> nous fournissons?</h2>
                    <p class="wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.2s">Offrez à votre entreprise la meilleure assistance à la croissance.</p>
                </div>
            </div>
        </div>
        <div class="row custom-gutters-16">
            <div class="col-xl-3 col-md-6 col-sm-6">
                <div class="single-service wow animated fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.1s">
                    <img src="../assets/img/service/project.png" alt="service">
                    <h6><a href="#">CRÉATION DE SITES WEB SUR MESURE</a></h6>
                    <p>Vous avez une idée digitale, un projet sur le web ? Contactez TRU  CREATIVE pour obtenir un devis sans engagement ou de plus amples informations !</p>
                    <div class="read-more">
                        <a href="#"><img src="../assets/img/service/arrow.png" alt="arrow"></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-sm-6">
                <div class="single-service wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.2s">
                    <img src="../assets/img/service/hiring.png" alt="service">
                    <h6><a href="#">CRÉATION D'APPLICATIONS MOBILES</a></h6>
                    <p>Notre équipe de spécialistes en développement d’applications mobiles met à votre service son savoir-faire pour la création d’applications répondant aux attentes de vos clients.</p>
                    <div class="read-more">
                        <a href="#"><img src="../assets/img/service/arrow.png" alt="arrow"></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-sm-6">
                <div class="single-service wow animated fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                    <img src="../assets/img/service/checklist.png" alt="service">
                    <h6><a href="#">CRÉATION D'APPLICATIONS ERP</a></h6>
                    <p>Avez vous besoin d'une application de gestion de votre Ecole, de gestion de votre stock, de votre station, transferts, associations, ... ? TRU  CREATIVE est votre Solution</p>
                    <div class="read-more">
                        <a href="#"><img src="../assets/img/service/arrow.png" alt="arrow"></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-sm-6">
                <div class="single-service wow animated fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                    <img src="../assets/img/service/analytics.png" alt="service">
                    <h6><a href="#">FORMATIONS</a></h6>
                    <p>Nous disposons d'une équipe passionnée et dynamique qui vous présente les outils pédagogiques nécessaires pour muscler vos connaissances et atteindre la performance souhaitée.</p>
                    <div class="read-more">
                        <a href="#"><img src="../assets/img/service/arrow.png" alt="arrow"></a>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>
<!-- service area End -->

<!-- competence area start -->
<div class="service-area sbtc-service-area pd-top-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-10">
                <div class="section-title text-center margin-bottom-90">
                    <h2 class="title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">NOS <span>COMPETENCES</span>?</h2>
                </div>
            </div>
        </div>
        <div class="row custom-gutters-16 justify-content-center">
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="single-service-2 style-two text-center">
                    <div class="thumb">
                        <img src="../assets/img/startup/service/1.png" alt="service">
                    </div>
                    <h6>CAHIER DES CHARGES</h6>
                    <p>TRU  CREATIVE vous accompagne dans la rédaction du cahier de charges qui est un document contractuel et annexé à l'offre signée qui décrit précisément le projet dans lequel vous vous lancez.</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="single-service-2 style-two text-center">
                    <div class="thumb">
                        <img src="../assets/img/startup/service/2.png" alt="service">
                    </div>
                    <h6>IDENTITÉ VISUELLE</h6>
                    <p>Toute entreprise se doit d'avoir une identité visuelle. Celle-ci doit permettre à votre entreprise d'augmenter sa notoriété et de mettre en valeur son activité et ses produits.</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="single-service-2 style-two text-center">
                    <div class="thumb">
                        <img src="../assets/img/startup/service/3.png" alt="service">
                    </div>
                    <h6>APPLICATIONS WEB</h6>
                    <p>Vous avez une idée digitale, un projet sur le web ? Contactez TRU  CREATIVE pour obtenir un devis sans engagement ou de plus amples informations </p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="single-service-2 style-two text-center">
                    <div class="thumb">
                        <img src="../assets/img/startup/service/4.png" alt="service">
                    </div>
                    <h6>APPLICATION MOBILE</h6>
                    <p>TRU  CREATIVE est une agence spécialisée dans la conception, le développement et la création d'applications mobiles innovantes, performantes et sur mesure</p>
                </div>
            </div>
        </div>
        <div class="row custom-gutters-16 justify-content-center">
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="single-service-2 style-two text-center">
                    <div class="thumb">
                        <img src="../assets/img/startup/service/1.png" alt="service">
                    </div>
                    <h6>SITE VITRINE</h6>
                    <p>Votre site Internet doit mettre en valeur votre marque, et vous démarquer de la concurrence.TRU  CREATIVE vous accompagne dans la réalisation de votre site, de sa conception jusqu'à sa mise en ligne.</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="single-service-2 style-two text-center">
                    <div class="thumb">
                        <img src="../assets/img/startup/service/2.png" alt="service">
                    </div>
                    <h6>SITE ECOMMERCE</h6>
                    <p>Pour la réalisation de vos sites ecommerces TRU  CREATIVE vous propose une formation pour la prise en main et la gestion du site, une fois en ligne, testé et approuvé par vos soins.</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="single-service-2 style-two text-center">
                    <div class="thumb">
                        <img src="../assets/img/startup/service/3.png" alt="service">
                    </div>
                    <h6>SOLUTIONS ERP</h6>
                    <p>Avez vous bésoin d'une solution pour gèrer vos magasins, vos écoles, vos stations, ...? TRU  CREATIVE est là pour vous offrir les meilleures solutions à vos bésoins.</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-sm-6">
                <div class="single-service-2 style-two text-center">
                    <div class="thumb">
                        <img src="../assets/img/startup/service/4.png" alt="service">
                    </div>
                    <h6>FORMATIONS</h6>
                    <p>TRU  CREATIVE agence de création des sites Web à Conakry, organise des ateliers de formation centrés sur les enjeux clés du digital et tous les leviers du Webmarketing.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- competence area end -->
<!-- goal area start -->
<div class="sbtc-goal-counter-area pd-top-115">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-9">
                <div class="thumb">
                    <img src="../assets/img/others/7.png" alt="goal">
                </div>
                <div class="goal-counter">
                    <div class="section-title">
                        <h2 class="title">Accomplir des <span>objectifs</span> <br> comme une équipe </h2>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="single-counter text-center">
                                <span class="counting count-num">50</span>
                                <h6>Nombre de client</h6>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="single-counter text-center">
                                <span class="counting count-num">49</span>
                                <h6>Client heureux</h6>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="single-counter text-center">
                                <span class="counting count-num">10</span>
                                <h6>Projet en cours</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- goal area End -->


<!-- testimonial area Start -->
<div class="testimonial-section sbs-testimonial-section pd-top-105 pd-bottom-120" style="background-image:url(assets/img/bg/1h3.png);">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-6 pd-xl-5 order-lg-12 align-self-center ">
                <div class="section-title style-two mb-0">
                    <h2 class="title">Que disent nos <span>clients? </span></h2>
                    <p>Une équipe compétente qui vous assiste en cas de besoin et toujours a votre disposition.<br>TRU CREATIVE une agence qui nous satisfait</p>
                </div>
            </div>
            <div class="col-lg-6 order-lg-1">
                <div class="sbs-testimonial-slider">
                    <div class="choose_slider">
                        <div class="choose_slider_items">
                            <ul id="testimonial-slider">
                                <li class="current_item">
                                    <div class="media">
                                        <img class="media-left" src="../assets/img/client/4.png" alt="client">
                                        <div class="media-body">
                                            <h6>Herve Tatinou</h6>
                                            <p class="designation">CEO MOCOR STUDIO </p>
                                            <span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <p>Une équipe compétente qui vous assiste en cas de besoin et toujours a votre disposition.<br>TRU CREATIVE une agence qui nous satisfait</p>
                                </li>
                                <li class="current_item">
                                    <div class="media">
                                        <img class="media-left" src="../assets/img/client/4.png" alt="client">
                                        <div class="media-body">
                                            <h6>Aissata Ibamie</h6>
                                            <p class="designation">CEO ASAAB THERMAL COOKER</p>
                                            <span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <p>Une équipe compétente qui vous assiste en cas de besoin et toujours a votre disposition.<br>TRU CREATIVE une agence qui nous satisfait</p>
                                </li>
                                <li class="current_item">
                                    <div class="media">
                                        <img class="media-left" src="../assets/img/client/4.png" alt="client">
                                        <div class="media-body">
                                            <h6>Yanpelda Irene</h6>
                                            <p class="designation">CEO Weey Energie et Eau</p>
                                            <span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <p>Une équipe compétente qui vous assiste en cas de besoin et toujours a votre disposition.<br>TRU CREATIVE une agence qui nous satisfait</p>
                                </li>
                                <li class="current_item">
                                    <div class="media">
                                        <img class="media-left" src="../assets/img/client/4.png" alt="client">
                                        <div class="media-body">
                                            <h6>Babani Djallo</h6>
                                            <p class="designation">CEO DMK</p>
                                            <span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <p>Une équipe compétente qui vous assiste en cas de besoin et toujours a votre disposition.<br>TRU CREATIVE une agence qui nous satisfait</p>
                                </li> 
                                <li class="current_item">
                                    <div class="media">
                                        <img class="media-left" src="../assets/img/client/4.png" alt="client">
                                        <div class="media-body">
                                            <h6>Marie Claire</h6>
                                            <p class="designation">Branding Idendity</p>
                                            <span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <p>Une équipe compétente qui vous assiste en cas de besoin et toujours a votre disposition.<br>TRU CREATIVE une agence qui nous satisfait</p>
                                </li> 
                                <li class="current_item">
                                    <div class="media">
                                        <img class="media-left" src="../assets/img/client/4.png" alt="client">
                                        <div class="media-body">
                                            <h6>Emmanuel FOKA</h6>
                                            <p class="designation">Manager Kacella Lab</p>
                                            <span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <p>Une équipe compétente qui vous assiste en cas de besoin et toujours a votre disposition.<br>TRU CREATIVE une agence qui nous satisfait</p>
                                </li> 
                            </ul>
                        </div>
                    </div>
                    <div class="sbs-arrowleft"><a id="btn_next" href="#"><i class="fa fa-long-arrow-left"></i></a></div>
                    <div class="sbs-arrowright"><a id="btn_prev" href="#"><i class="fa fa-long-arrow-right"></i></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- testimonial area End -->

<!-- newsletter area Start -->
<!-- <div class="newsletter-area mg-top-110">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-10 text-center">
                <div class="section-title">
                    <h2 class="title">Abonnez vous à notre <span>Newslatter</span></h2>
                    <p>Offrez à votre entreprise la meilleure assistance à la croissance.</p>
                                    </div>
                <div class="newsletter-subcribe">
                    <form action="" class="subcribe-form" id="news-subcribeform" method="post" accept-charset="utf-8">
                        <div class="form-group">
                            <input type="text" class="form-control" required="required" placeholder="Votre E-mail..." name="email" required="">
                            <input type="hidden" name="form1" value="home" class="form-control" >
                            <button type="submit" class="btn-green subcribe-submit">Envoyer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- newsletter area End -->

<!-- footer area start -->

<!-- footer area start -->
<footer class="footer-area mg-top-120" style="background-image:url(../../localhost/tru/assets/img/bg/footer.html);">
    <div class="footer-top pd-top-120 padding-bottom-40">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-9 col-lg-11">
                    <div class="footer-widget widget text-center">
                        <div class="footer_top_widget">
                            <a href="welcome.html" class="footer-logo"> 
                                <img src="../assets/img/logo.png" alt="footer logo">
                            </a>
                            <p>Notre équipe d'assistance sera toujours à votre disposition pour tout besoin</p>
                        </div>
                    </div>
                    <div class="footer-widget widget widget_nav_menu text-center">
                        <ul>
                            <li><a href="welcome.html">Accueil</a></li>
                            <li><a href="welcome/formation.html">Formation</a></li>
                            <li><a href="welcome/realisation.html">Realisation</a></li>
                            <li><a href="../team.html">Team</a></li>
                            <li><a href="welcome/contact.html">Contact</a></li>
                        </ul>
                    </div>
                    <div class="copyright-inner">
                        <div class="row custom-gutters-16">
                            <div class="col-lg-7">
                                <div class="copyright-text">
                                    &copy;Tous droits réservés<a href="https://codingeek.net/" target="_blank"><i class="fa fa-heart"></i><span>TRU CREATIVE.</span></a>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <ul class="social-icon text-right">
                                   
                                    <li>
                                        <a class="facebook" href="" target="_blank"><i class="fa fa-facebook  "></i></a>
                                    </li>
                                    <li>
                                        <a class="twitter" href="" target="_blank"><i class="fa fa-twitter  "></i></a>
                                    </li>
                                    <li>
                                        <a class="linkedin" href="" target="_blank"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer area end -->

<!-- back to top area start -->
<div class="back-to-top">
    <span class="back-top"><i class="fa fa-angle-up"></i></span>
</div>
<!-- back to top area end -->


    <!-- jquery -->
    <script src="../assets/js/jquery-2.2.4.min.js"></script>
    <!-- popper -->
    <script src="../assets/js/popper.min.js"></script>
    <!-- bootstrap -->
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- magnific popup -->
    <script src="../assets/js/jquery.magnific-popup.js"></script>
    <!-- wow -->
    <script src="../assets/js/wow.min.js"></script>
    <!-- owl carousel -->
    <script src="../assets/js/owl.carousel.min.js"></script>
    <!-- slick slider -->
    <script src="../assets/js/slick.js"></script>
    <!-- cssslider slider -->
    <script src="../assets/js/jquery.cssslider.min.js"></script>
    <!-- waypoint -->
    <script src="../assets/js/waypoints.min.js"></script>
    <!-- counterup -->
    <script src="../assets/js/jquery.counterup.min.js"></script>
    <!-- imageloaded -->
    <script src="../assets/js/imagesloaded.pkgd.min.js"></script>
    <!-- isotope -->
    <script src="../assets/js/isotope.pkgd.min.js"></script>
    <!-- world map -->
    <script src="../assets/js/worldmap-libs.js"></script>
    <script src="../assets/js/worldmap-topojson.js"></script>
    <script src="../assets/js/mediaelement.min.js"></script>
     <!-- main js -->
    <script src="../assets/js/main.js"></script>

</body>


</html>
<!-- footer area end -->
