<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TRU  CREATIVE</title>
    
    <!-- animate -->
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="../../assets/css/magnific-popup.css">
    <!-- owl carousel -->
    <link rel="stylesheet" href="../../assets/css/owl.carousel.min.css">
    <!-- icons -->
    <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../assets/css/line-awesome.min.css">
    <link rel="stylesheet" href="../../assets/css/themify-icons.css">
    <link rel="stylesheet" href="../../assets/css/flaticon.css">
    <!-- slick slider -->
    <link rel="stylesheet" href="../../assets/css/slick.css">
    <!-- animated slider -->
    <link rel="stylesheet" href="../../assets/css/animated-slider.css">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- responsive Stylesheet -->
    <link rel="stylesheet" href="../../assets/css/responsive.css">

</head>
<body>

<!-- preloader area start -->
<div class="preloader" id="preloader">
    <div class="preloader-inner">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<!-- preloader area end -->


<!-- navbar area start -->
<nav class="navbar navbar-area navbar-expand-lg nav-style-01">
    <div class="container nav-container">
        <div class="responsive-mobile-menu">
            <div class="logo-wrapper mobile-logo">
                <a href="../welcome.html" class="logo">
                   <img src="../../assets/img/logo.png" alt="logo">
                </a>
            </div>
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#Riyaqas_main_menu" 
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggle-icon">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="Riyaqas_main_menu">
            <div class="logo-wrapper desktop-logo">
                <a href="../welcome.html" class="logo">
                    <img src="../../assets/img/logo.png" alt="logo">
                </a>
            </div>
            <ul class="navbar-nav">
                <li >
                    
                    <a href="../welcome.html">Accueil</a>
                
                </li>
                
                <li>
                    <a href="formation.html">Formations</a>
                </li>
                <li>
                     <a href="realisation.html">Realisations</a>
                    
                </li>
               
                    <li><a href="../team.html">Team</a></li>
               
                <li>
                    <a href="contact.html">Contact</a>
                </li>
            </ul>
        </div>
        <div class="nav-right-content">
            <a href="devis.html" class="btn btn-radius btn-red mr-2 mb-2">Demander un devis de site web</a>
        </div>
    </div>
</nav>
<!-- navbar area end -->

<!-- breadcrumb area start -->
<div class="breadcrumb-area" style="background-image:url('../../assets/img/page-title-bg.png');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Realisation TRU  CREATIVE</h1>
                    <!-- <ul class="page-list">
                        <li><a href="../welcome.html">Accueil</a></li>
                        <li>Realisation</li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<!-- realisation start -->
<div class="work-processing-area pd-top-120">
    <div class="container">
        <div class="row justify-content-center custom-gutters-16 single-work-processing">
            <div class="col-xl-5 col-md-6 order-lg-12">
                <div class="thumb swp-right-thumb">
                    <img src="../../assets/img/realisation/together.png" alt="img">
                </div>
            </div>
            <div class="col-xl-4 col-md-6 order-lg-1 desktop-center-item">
                <div class="work-processing-details">   
                    <div class="section-title style-four">
                        <h3 class="title">Réalisation du site du mouvement <span>TOGETHER25</span> </h3>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center custom-gutters-16 single-work-processing">
            <div class="col-xl-5 col-md-6">
                <div class="thumb">
                    <img src="../../assets/img/realisation/sungku.png" alt="img">
                </div>
            </div>
            <div class="col-xl-4 col-md-6 desktop-center-item">
                <div class="work-processing-details">   
                    <div class="section-title style-four">
                        <h2 class="title"><span>Site du projet</span> SUNGKU à Maroua </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center custom-gutters-16 single-work-processing">
            <div class="col-xl-5 col-md-6 order-lg-12">
                <div class="thumb swp-right-thumb">
                    <img src="../../assets/img/realisation/ars.png" alt="img">
                </div>
            </div>
            <div class="col-xl-4 col-md-6 order-lg-1 desktop-center-item">
                <div class="work-processing-details">   
                    <div class="section-title style-four">
                        <h2 class="title">Realisation du site d'une <span>ferme</span> </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center custom-gutters-16 single-work-processing">
            <div class="col-xl-5 col-md-6">
                <div class="thumb">
                    <img src="../../assets/img/realisation/gescom.png" alt="img">
                </div>
            </div>
            <div class="col-xl-4 col-md-6 desktop-center-item">
                <div class="work-processing-details">   
                    <div class="section-title style-four">
                        <h2 class="title">La mise en place d'une application de gestion commérciale <span>GESCOM</span></h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="row justify-content-center custom-gutters-16 single-work-processing">
            <div class="col-xl-5 col-md-6 order-lg-12">
                <div class="thumb swp-right-thumb">
                    <img src="../../assets/img/realisation/khoti.png" alt="img">
                </div>
            </div>
            <div class="col-xl-4 col-md-6 order-lg-1 desktop-center-item">
                <div class="work-processing-details">   
                    <div class="section-title style-four">
                        <h2 class="title">le site de la marque <span>KHoTI</span> </h2>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</div>
<!-- realisation End -->

<!-- client area start -->
<div class="client-area pd-top-112">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-9">
                <div class="section-title style-two text-center">
                    <h2 class="title">Toutes les <span>Realisations </span></h2>
                </div>
            </div>
        </div>
        <div class="client-slider">
            <div class="client-slider-item">
                <div class="media">
                    <img src="../../assets/img/realisation/together.png" alt="client">
                    <div class="media-body">
                        <h6>Site web de together25</h6>
                    </div>
                </div>
                <p class="client-content">Réalisation du site du mouvement <span>TOGETHER25</span></p>
                <a href="http://together25.org/" class="btn btn-blue">Voir le site</a>
            </div>
            <!-- <div class="client-slider-item">
                <div class="media">
                    <img src="../../assets/img/realisation/khoti.png" alt="client">
                    <div class="media-body">
                        <h6>KHoTI</h6>
                    </div>
                </div>
                <p class="client-content">site web de la marque KHoTI.</p>
                <a href="http://together25.org/" class="btn btn-blue">Voir le site</a>
            </div> -->
            <div class="client-slider-item">
                <div class="media">
                    <img src="../../assets/img/realisation/sungku.png" alt="client">
                    <div class="media-body">
                        <h6>SUNGKU</h6>
                    </div>
                </div>
                <p class="client-content">Site d'une association pour les ressortissans Guinéens aux Etat-unis</p>
                <a href="https://sungku.org/" class="btn btn-blue">Voir le site</a>
            </div>
            <div class="client-slider-item">
                <div class="media">
                    <img src="../../assets/img/realisation/gescom.png" alt="client">
                    <div class="media-body">
                        <h6>GESCOM</h6>
                    </div>
                </div>
                <p class="client-content">La mise en place d'une application de gestion commérciale au service des commerçants</p>
                <a href="http://together25.org/" class="btn btn-blue">Voir le site</a>
            </div>
            <div class="client-slider-item">
                <div class="media">
                    <img src="../../assets/img/realisation/ferme.png" alt="client">
                    <div class="media-body">
                        <h6>AfribelGroup</h6>
                    </div>
                </div>
                <p class="client-content">Site web de la ferme Afribel Group.</p>
                <a href="http://together25.org/" class="btn btn-blue">Voir le site</a>
            </div>
            <!-- <div class="client-slider-item">
                <div class="media">
                    <img src="../../assets/img/realisation/ledjely.html" alt="client">
                    <div class="media-body">
                        <h6>ledjely</h6>
                    </div>
                </div>
                <p class="client-content">Application mobile </p>
                <a href="http://together25.org/" class="btn btn-blue">Voir le site</a>
            </div> -->
        </div>
    </div>
</div>
<!-- client area End -->

<!-- footer area start -->
<footer class="footer-area mg-top-120" style="background-image:url(../../../localhost/tru/assets/img/bg/footer.html);">
    <div class="footer-top pd-top-120 padding-bottom-40">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-9 col-lg-11">
                    <div class="footer-widget widget text-center">
                        <div class="footer_top_widget">
                            <a href="../welcome.html" class="footer-logo"> 
                                <img src="../../assets/img/logo.png" alt="footer logo">
                            </a>
                            <p>Notre équipe d'assistance sera toujours à votre disposition pour tout besoin</p>
                        </div>
                    </div>
                    <div class="footer-widget widget widget_nav_menu text-center">
                        <ul>
                            <li><a href="welcome.html">Accueil</a></li>
                            <li><a href="welcome/formation.html">Formation</a></li>
                            <li><a href="welcome/realisation.html">Realisation</a></li>
                            <li><a href="../team.html">Team</a></li>
                            <li><a href="welcome/contact.html">Contact</a></li>
                        </ul>
                    </div>
                    <div class="copyright-inner">
                        <div class="row custom-gutters-16">
                            <div class="col-lg-7">
                                <div class="copyright-text">
                                    &copy;Tous droits réservés<a href="https://codingeek.net/" target="_blank"><i class="fa fa-heart"></i><span>TRU  CREATIVE.</span></a>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <ul class="social-icon text-right">
                                   
                                    <li>
                                        <a class="facebook" href="y" target="_blank"><i class="fa fa-facebook  "></i></a>
                                    </li>
                                    <li>
                                        <a class="twitter" href="" target="_blank"><i class="fa fa-twitter  "></i></a>
                                    </li>
                                    <li>
                                        <a class="linkedin" href="" target="_blank"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer area end -->

<!-- back to top area start -->
<div class="back-to-top">
    <span class="back-top"><i class="fa fa-angle-up"></i></span>
</div>
<!-- back to top area end -->


    <!-- jquery -->
    <script src="../../assets/js/jquery-2.2.4.min.js"></script>
    <!-- popper -->
    <script src="../../assets/js/popper.min.js"></script>
    <!-- bootstrap -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <!-- magnific popup -->
    <script src="../../assets/js/jquery.magnific-popup.js"></script>
    <!-- wow -->
    <script src="../../assets/js/wow.min.js"></script>
    <!-- owl carousel -->
    <script src="../../assets/js/owl.carousel.min.js"></script>
    <!-- slick slider -->
    <script src="../../assets/js/slick.js"></script>
    <!-- cssslider slider -->
    <script src="../../assets/js/jquery.cssslider.min.js"></script>
    <!-- waypoint -->
    <script src="../../assets/js/waypoints.min.js"></script>
    <!-- counterup -->
    <script src="../../assets/js/jquery.counterup.min.js"></script>
    <!-- imageloaded -->
    <script src="../../assets/js/imagesloaded.pkgd.min.js"></script>
    <!-- isotope -->
    <script src="../../assets/js/isotope.pkgd.min.js"></script>
    <!-- world map -->
    <script src="../../assets/js/worldmap-libs.js"></script>
    <script src="../../assets/js/worldmap-topojson.js"></script>
    <script src="../../assets/js/mediaelement.min.js"></script>
     <!-- main js -->
    <script src="../../assets/js/main.js"></script>

</body>


</html>