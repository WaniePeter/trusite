<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TRU  CREATIVE</title>
    
    <!-- animate -->
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="../../assets/css/magnific-popup.css">
    <!-- owl carousel -->
    <link rel="stylesheet" href="../../assets/css/owl.carousel.min.css">
    <!-- icons -->
    <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../assets/css/line-awesome.min.css">
    <link rel="stylesheet" href="../../assets/css/themify-icons.css">
    <link rel="stylesheet" href="../../assets/css/flaticon.css">
    <!-- slick slider -->
    <link rel="stylesheet" href="../../assets/css/slick.css">
    <!-- animated slider -->
    <link rel="stylesheet" href="../../assets/css/animated-slider.css">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- responsive Stylesheet -->
    <link rel="stylesheet" href="../../assets/css/responsive.css">

</head>
<body>

<!-- preloader area start -->
<div class="preloader" id="preloader">
    <div class="preloader-inner">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<!-- preloader area end -->


<!-- navbar area start -->
<nav class="navbar navbar-area navbar-expand-lg nav-style-01">
    <div class="container nav-container">
        <div class="responsive-mobile-menu">
            <div class="logo-wrapper mobile-logo">
                <a href="../welcome.html" class="logo">
                   <img src="../../assets/img/logo.png" alt="logo">
                </a>
            </div>
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#Riyaqas_main_menu" 
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggle-icon">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="Riyaqas_main_menu">
            <div class="logo-wrapper desktop-logo">
                <a href="../welcome.html" class="logo">
                    <img src="../../assets/img/logo.png" alt="logo">
                </a>
            </div>
            <ul class="navbar-nav">
                <li >
                    
                    <a href="../welcome.html">Accueil</a>
                
                </li>
                
                <li>
                    <a href="formation.html">Formations</a>
                </li>
                <li>
                     <a href="realisation.html">Realisations</a>
                    
                </li>
                <li><a href="../team.html">Team</a> </li>
                <li>
                    <a href="contact.html">Contact</a>
                </li>
            </ul>
        </div>
        <div class="nav-right-content">
            <a href="devis.html" class="btn btn-radius btn-red mr-2 mb-2">Demander un devis de site web</a>
        </div>
    </div>
</nav>
<!-- navbar area end --><!-- breadcrumb area start -->
<div class="breadcrumb-area" style="background-image:url('../../assets/img/page-title-bg.png');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Contact</h1>
                    <!-- <ul class="page-list">
                        <li><a href="../welcome.html">Accueil</a></li>
                        <li>Contact</li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<!-- contact form start -->
<div class="contact-form-area pd-top-112">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8">
                <div class="section-title text-center w-100">
                    <h2 class="title">Envoyez votre <span>demande</span></h2>
                    <p>CONTACTEZ – NOUS, ET BÂTISSONS ENSEMBLE VOTRE PROJET !</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-5">
                <img src="../../assets/img/others/21.png" alt="blog">
            </div>
            <div class="col-lg-7 offset-xl-1">
                <div class="text text-center">
                                    </div>
                <form action="" class="riyaqas-form-wrap mt-5 mt-lg-0" method="post" accept-charset="utf-8">
                    <div class="row custom-gutters-16">
                        <div class="col-md-6">
                            <div class="single-input-wrap">
                                <input type="text" name="nom" class="single-input">
                                <label>Nom</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="single-input-wrap">
                                <input type="text" name="email" required="required" class="single-input">
                                <label>E-mail</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="single-input-wrap">
                                <input type="text" name="objet" class="single-input">
                                <label>Objet</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="single-input-wrap">
                                <textarea class="single-input textarea" name="msge" cols="20"></textarea>
                                <label class="single-input-label">Message</label>
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-red mt-0">Envoyer</button>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- map area start -->
<div class="map-area pd-top-120">
    <div class="container">
        <div class="map-area-wrap">
            <div class="row no-gutters">
                <div class="col-lg-8">
                    <div id="map"></div>
                </div>  
                <div class="col-lg-4 desktop-center-item">
                    <div>
                        <div class="contact-info">
                            <h4 class="title">Informations de contact:</h4>
                            <p>Addresse: Maroua, commune Maroua 1er</p>
                            <p><span>Mobile:</span> +237 678 758 976</p>
                            <p><span>E-mail:</span> tru@gmail.com</p>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
<!-- map area End -->



<!-- footer area start -->
<footer class="footer-area mg-top-120" style="background-image:url(../../../localhost/tru/assets/img/bg/footer.html);">
    <div class="footer-top pd-top-120 padding-bottom-40">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-9 col-lg-11">
                    <div class="footer-widget widget text-center">
                        <div class="footer_top_widget">
                            <a href="../welcome.html" class="footer-logo"> 
                                <img src="../../assets/img/logo.png" alt="footer logo">
                            </a>
                            <p>Notre équipe d'assistance sera toujours à votre disposition pour tout besoin</p>
                        </div>
                    </div>
                    <div class="footer-widget widget widget_nav_menu text-center">
                        <ul>
                            <li><a href="../welcome.html">Accueil</a></li>
                            <li><a href="formation.html">Formation</a></li>
                            <li><a href="realisation.html">Realisation</a></li>
                            <li><a href="../team.html">Team</a> </li>
                            <li><a href="contact.html">Contact</a></li>
                        </ul>
                    </div>
                    <div class="copyright-inner">
                        <div class="row custom-gutters-16">
                            <div class="col-lg-7">
                                <div class="copyright-text">
                                    &copy;Tous droits réservés<a href="https://codingeek.net/" target="_blank"><i class="fa fa-heart"></i><span>TRU  CREATIVE.</span></a>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <ul class="social-icon text-right">
                                   
                                    <li>
                                        <a class="facebook" href="" target="_blank"><i class="fa fa-facebook  "></i></a>
                                    </li>
                                    <li>
                                        <a class="twitter" href="" target="_blank"><i class="fa fa-twitter  "></i></a>
                                    </li>
                                    <li>
                                        <a class="linkedin" href="" target="_blank"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer area end -->

<!-- back to top area start -->
<div class="back-to-top">
    <span class="back-top"><i class="fa fa-angle-up"></i></span>
</div>
<!-- back to top area end -->


    <!-- jquery -->
    <script src="../../assets/js/jquery-2.2.4.min.js"></script>
    <!-- popper -->
    <script src="../../assets/js/popper.min.js"></script>
    <!-- bootstrap -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <!-- magnific popup -->
    <script src="../../assets/js/jquery.magnific-popup.js"></script>
    <!-- wow -->
    <script src="../../assets/js/wow.min.js"></script>
    <!-- owl carousel -->
    <script src="../../assets/js/owl.carousel.min.js"></script>
    <!-- slick slider -->
    <script src="../../assets/js/slick.js"></script>
    <!-- cssslider slider -->
    <script src="../../assets/js/jquery.cssslider.min.js"></script>
    <!-- waypoint -->
    <script src="../../assets/js/waypoints.min.js"></script>
    <!-- counterup -->
    <script src="../../assets/js/jquery.counterup.min.js"></script>
    <!-- imageloaded -->
    <script src="../../assets/js/imagesloaded.pkgd.min.js"></script>
    <!-- isotope -->
    <script src="../../assets/js/isotope.pkgd.min.js"></script>
    <!-- world map -->
    <script src="../../assets/js/worldmap-libs.js"></script>
    <script src="../../assets/js/worldmap-topojson.js"></script>
    <script src="../../assets/js/mediaelement.min.js"></script>
     <!-- main js -->
    <script src="../../assets/js/main.js"></script>

</body>


</html>
