<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TRU CREATIVE</title>
    
    <!-- animate -->
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="../../assets/css/magnific-popup.css">
    <!-- owl carousel -->
    <link rel="stylesheet" href="../../assets/css/owl.carousel.min.css">
    <!-- icons -->
    <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../assets/css/line-awesome.min.css">
    <link rel="stylesheet" href="../../assets/css/themify-icons.css">
    <link rel="stylesheet" href="../../assets/css/flaticon.css">
    <!-- slick slider -->
    <link rel="stylesheet" href="../../assets/css/slick.css">
    <!-- animated slider -->
    <link rel="stylesheet" href="../../assets/css/animated-slider.css">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- responsive Stylesheet -->
    <link rel="stylesheet" href="../../assets/css/responsive.css">

</head>
<body>

<!-- preloader area start -->
<div class="preloader" id="preloader">
    <div class="preloader-inner">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<!-- preloader area end -->


<!-- navbar area start -->
<nav class="navbar navbar-area navbar-expand-lg nav-style-01">
    <div class="container nav-container">
        <div class="responsive-mobile-menu">
            <div class="logo-wrapper mobile-logo">
                <a href="../welcome.html" class="logo">
                   <img src="../../assets/img/logo.png" alt="logo">
                </a>
            </div>
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#Riyaqas_main_menu" 
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggle-icon">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="Riyaqas_main_menu">
            <div class="logo-wrapper desktop-logo">
                <a href="../welcome.html" class="logo">
                    <img src="../../assets/img/logo.png" alt="logo">
                </a>
            </div>
            <ul class="navbar-nav">
                <li >
                    
                    <a href="../welcome.html">Accueil</a>
                
                </li>
                
                <li>
                    <a href="formation.html">Formations</a>
                </li>
                <li>
                     <a href="realisation.html">Realisations</a>
                    
                </li>
                <li>
                    <a href="../team.html">Team</a>
               </li>
                <li>
                    <a href="contact.html">Contact</a>
                </li>
            </ul>
        </div>
        <div class="nav-right-content">
            <a href="devis.html" class="btn btn-radius btn-red mr-2 mb-2">Demander un devis de site web</a>
        </div>
    </div>
</nav>
<!-- navbar area end --><!-- breadcrumb area start -->
<div class="breadcrumb-area" style="background-image:url('../../assets/img/page-title-bg.png');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Devis TRU CREATIVE</h1>
                    <!-- <ul class="page-list">
                        <li><a href="../welcome.html">Accueil</a></li>
                        <li>Devis</li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<!-- devis start -->
<div class="job-listing-page pd-top-190">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-10">
                <div class="section-title text-center">
                    <h2 class="title">Demande de devis</h2>
                    <h3>Simuler un devis de création de site Internet.</h3>
                                    </div>
                <div class="job-apply-area">
                    
                        <form action="" class="riyaqas-form-wrap" method="post" accept-charset="utf-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="single-input-wrap">
                                    <label>Quel est le type de votre structure?</label>
                                    <select class="single-input" name="structure" required cols="20">
                                        <option> </option>
                                        <option value="particulier">Particulier</option>
                                        <option value="association">Association</option>
                                        <option value="independant ou TPE">independant ou TPE</option>
                                        <option value="pme">PME</option>
                                        <option value="grande entreprise ou groupe">Grande entreprise ou Groupe</option>
                                        <option value="organisme">Organisme</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-input-wrap">
                                    <select class="single-input" required name="domaine" cols="20">
                                        <option> </option>
                                        <option value="oui">Oui</option>
                                        <option value="non">Non</option>
                                    </select>
                                    <label>Avez-vous déjà un nom de domaine?</label>
                                </div>
                            </div>
                             <div class="col-md-12">
                                <div class="single-input-wrap">
                                    <select class="single-input" required name="hebergement" cols="20">
                                        <option> </option>
                                        <option value="oui">Oui</option>
                                        <option value="non">Non</option>
                                    </select>
                                    <label>Disposez-vous d'un serveur d'hébergement?</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-input-wrap">
                                    <select class="single-input" required name="logo" cols="20">
                                        <option> </option>
                                        <option value="oui">Oui</option>
                                        <option value="non">Non</option>
                                    </select>
                                    <label>Avez-vous un logo?</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-input-wrap">
                                    <select class="single-input" required name="charteGraphique" cols="20">
                                        <option> </option>
                                        <option value="oui">Oui</option>
                                        <option value="non">Non</option>
                                    </select>
                                    <label>Avez-vous déjà une charte graphique?</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-input-wrap">
                                    <label>Quelle sera en moyenne la récurrence des mises à jour de contenu du site ? </label>
                                    <select class="single-input" required name="recurrence" cols="20">
                                        <option> </option>
                                        <option value="aucune">Aucune</option>
                                        <option value="annuelle">Annuelle</option>
                                        <option value="trimestrielle">Trimestrielle</option>
                                        <option value="semestrielle">Semestrielle</option>
                                        <option value="mensuelle">Mensuelle</option>
                                        <option value="bimensuelle">Bimensuelle</option>
                                        <option value="hebdomadaire">Hebdomadaire</option>
                                        <option value="journaliere">Journalière</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-input-wrap">
                                    <select class="single-input" required name="texte" cols="20">
                                        <option> </option>
                                        <option value="oui">Oui</option>
                                        <option value="non">Non</option>
                                    </select>
                                    <label>Fournissez-vous des textes pour les pages de votre site Internet ?</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-input-wrap">
                                    <select class="single-input" required name="medias" cols="20">
                                        <option> </option>
                                        <option value="oui">Oui</option>
                                        <option value="non">Non</option>
                                    </select>
                                    <label>Fournissez-vous des images ou d'autres médias pour les pages de votre site ?</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-input-wrap">
                                    <label>A combien estimez-vous le nombre de pages de votre futur site Internet ?</label>
                                    <select class="single-input" required name=nbpage cols="20">
                                        <option> </option>
                                        <option value="moins10">moins de 10</option>
                                        <option value="10 à 50">de 10 à 50</option>
                                        <option value="50 à 100">de 50 à 100 </option>
                                        <option value="100 à 200">de 100 à 200 </option>
                                        <option value="200 à 500">de 200 à 500</option>
                                        <option value="plus de 500">plus de 500</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-input-wrap">
                                    <select class="single-input" required name="internaute" cols="20">
                                        <option> </option>
                                        <option value="oui">Oui</option>
                                        <option value="non">Non</option>
                                    </select>
                                    <label>Les internautes pourront-ils s'inscrire ou se connecter à votre site pour gérer des informations ?</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-input-wrap">
                                    <label>Dans combien de langues supplémentaires le site devra t'il être traduit ?</label>
                                    <select class="single-input" required name="langue" cols="20">
                                        <option> </option>
                                        <option value="aucune">aucune</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="plus de 10">plus de 10</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                
                                <div>
                                    <div class="row">
                                        <h6 class="text text-center">Quels modules souhaitez-vous voir apparaître sur votre site Internet ?</h6>
                                    </div>
                                    <hr>
                                    <div class="row" style="">
                                        <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Administration">
                                            <label for="md">Administration</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Actualite">
                                            <label for="md">Actualité</label>
                                        </div>
                                         <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Formulaire de contact">
                                            <label for="md">Formulaire de contact</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Formulaire de devis">
                                            <label for="md">Formulaire de devis</label>
                                        </div>
                                         <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Catalogue ou annuaire">
                                            <label for="md">Catalogue ou annuaire</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="E-commerce">
                                            <label for="md">E-commerce</label>
                                        </div>
                                         <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Paiement en ligne">
                                            <label for="md">Paiement en ligne</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Galerie photo ou vidéo">
                                            <label for="md">Galerie photo ou vidéo</label>
                                        </div>
                                         <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Bouton de partage social">
                                            <label for="md">Bouton de partage social</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Moteur de recherche">
                                            <label for="md">Moteur de recherche</label>
                                        </div>
                                         <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Vote et commentaire ">
                                            <label for="md">Vote et commentaire</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Sondage">
                                            <label for="md">Sondage</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Chat et forum">
                                            <label for="md">Chat et forum</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Livre d'or">
                                            <label for="md">Livre d'or</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Calendrier ou planning">
                                            <label for="md">Calendrier ou planning</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Newsletter">
                                            <label for="md">Newsletter</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="checkbox"  name="modules[]" id="md" value="Statistiques">
                                            <label for="md">Statistiques</label>
                                        </div>
                                        
                                        

                                    </div>
                                </div>
                            </div>
                            <br>
                             <div class="col-md-12">
                                <div class="single-input-wrap">
                                    <select class="single-input" required name="nbmodule" cols="20">
                                        <option> </option>
                                        <option value="aucun">aucun</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="plus de 10">plus de 10</option>
                                    </select>
                                    <label>Combien d'autres modules pensez-vous qu'il faudra concevoir ?</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-input-wrap">
                                    <textarea class="single-input" required name="projet" cols="20" rows="10"></textarea>
                                    <label class="text text-danger">Décrivez votre projet</label>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="section-title text-center">
                            <h3>Indiquez vos coordonnées</h3>
                        </div>
                        <div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="single-input-wrap">
                                        <input type="text" required name="prenom" class="single-input">
                                        <label>Prenom</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="single-input-wrap">
                                        <input type="text" required name="nom" class="single-input">
                                        <label>Nom</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="single-input-wrap">
                                        <input type="text" required name="soc" class="single-input">
                                        <label>Sociéte</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="single-input-wrap">
                                        <input type="text" required name="adr" class="single-input">
                                        <label>Adresse</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="single-input-wrap">
                                        <input type="text"  name="cp" class="single-input">
                                        <label>Code Postal</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="single-input-wrap">
                                        <input type="text" required name="ville" class="single-input">
                                        <label>Ville</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="single-input-wrap">
                                        <input type="tel" required name="tel" class="single-input">
                                        <label>Téléphone</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="single-input-wrap">
                                        <input type="text" required name="mail" class="single-input">
                                        <label>E-mail</label>
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                  <button  type="submit" class="btn btn-blue">Enregistrer</button>
                                </div> 
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- devis End -->



<!-- footer area start -->
<footer class="footer-area mg-top-120" style="background-image:url(../localhost/tru/assets/img/bg/footer.html);">
    <div class="footer-top pd-top-120 padding-bottom-40">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-9 col-lg-11">
                    <div class="footer-widget widget text-center">
                        <div class="footer_top_widget">
                            <a href="index.php/welcome.html" class="footer-logo"> 
                                <img src="../../assets/img/logo.png" alt="footer logo">
                            </a>
                            <p>Notre équipe d'assistance sera toujours à votre disposition pour tout besoin</p>
                        </div>
                    </div>
                    <div class="footer-widget widget widget_nav_menu text-center">
                        <ul>
                            <li><a href="../welcome.html">Accueil</a></li>
                            <li><a href="formation.html">Formation</a></li>
                            <li><a href="realisation.html">Realisation</a></li>
                            <li><a href="../team.html">Team</a> </li>
                            <li><a href="contact.html">Contact</a></li>
                        </ul>
                    </div>
                    <div class="copyright-inner">
                        <div class="row custom-gutters-16">
                            <div class="col-lg-7">
                                <div class="copyright-text">
                                    &copy;Tous droits réservés<a href="https://codingeek.net/" target="_blank"><i class="fa fa-heart"></i><span>TRU.</span></a>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <ul class="social-icon text-right">
                                   
                                    <li>
                                        <a class="facebook" href="" target="_blank"><i class="fa fa-facebook  "></i></a>
                                    </li>
                                    <li>
                                        <a class="twitter" href="" target="_blank"><i class="fa fa-twitter  "></i></a>
                                    </li>
                                    <li>
                                        <a class="linkedin" href="" target="_blank"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer area end -->

<!-- back to top area start -->
<div class="back-to-top">
    <span class="back-top"><i class="fa fa-angle-up"></i></span>
</div>
<!-- back to top area end -->


    <!-- jquery -->
    <script src="../../assets/js/jquery-2.2.4.min.js"></script>
    <!-- popper -->
    <script src="../../assets/js/popper.min.js"></script>
    <!-- bootstrap -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <!-- magnific popup -->
    <script src="../../assets/js/jquery.magnific-popup.js"></script>
    <!-- wow -->
    <script src="../../assets/js/wow.min.js"></script>
    <!-- owl carousel -->
    <script src="../../assets/js/owl.carousel.min.js"></script>
    <!-- slick slider -->
    <script src="../../assets/js/slick.js"></script>
    <!-- cssslider slider -->
    <script src="../../assets/js/jquery.cssslider.min.js"></script>
    <!-- waypoint -->
    <script src="../../assets/js/waypoints.min.js"></script>
    <!-- counterup -->
    <script src="../../assets/js/jquery.counterup.min.js"></script>
    <!-- imageloaded -->
    <script src="../../assets/js/imagesloaded.pkgd.min.js"></script>
    <!-- isotope -->
    <script src="../../assets/js/isotope.pkgd.min.js"></script>
    <!-- world map -->
    <script src="../../assets/js/worldmap-libs.js"></script>
    <script src="../../assets/js/worldmap-topojson.js"></script>
    <script src="../../assets/js/mediaelement.min.js"></script>
     <!-- main js -->
    <script src="../../assets/js/main.js"></script>

</body>


</html>