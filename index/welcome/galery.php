<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codingeek.net/html/riyaqas/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 17 Feb 2020 03:46:41 GMT -->
<!-- Added by HTTrack -->
<!-- Mirrored from altgras.com/index.php/welcome/galery by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 15 Aug 2020 09:12:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ALTGRAS</title>
    
    <!-- animate -->
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="../../assets/css/magnific-popup.css">
    <!-- owl carousel -->
    <link rel="stylesheet" href="../../assets/css/owl.carousel.min.css">
    <!-- icons -->
    <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../assets/css/line-awesome.min.css">
    <link rel="stylesheet" href="../../assets/css/themify-icons.css">
    <link rel="stylesheet" href="../../assets/css/flaticon.css">
    <!-- slick slider -->
    <link rel="stylesheet" href="../../assets/css/slick.css">
    <!-- animated slider -->
    <link rel="stylesheet" href="../../assets/css/animated-slider.css">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- responsive Stylesheet -->
    <link rel="stylesheet" href="../../assets/css/responsive.css">

</head>
<body>

<!-- preloader area start -->
<div class="preloader" id="preloader">
    <div class="preloader-inner">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<!-- preloader area end -->


<!-- navbar area start -->
<nav class="navbar navbar-area navbar-expand-lg nav-style-01">
    <div class="container nav-container">
        <div class="responsive-mobile-menu">
            <div class="logo-wrapper mobile-logo">
                <a href="../welcome.html" class="logo">
                   <img src="../../assets/img/logo.png" alt="logo">
                </a>
            </div>
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#Riyaqas_main_menu" 
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggle-icon">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="Riyaqas_main_menu">
            <div class="logo-wrapper desktop-logo">
                <a href="../welcome.html" class="logo">
                    <img src="../../assets/img/logo.png" alt="logo">
                </a>
            </div>
            <ul class="navbar-nav">
                <li >
                    
                    <a href="../welcome.html">Accueil</a>
                
                </li>
                
                <li>
                    <a href="formation.html">Formations</a>
                </li>
                <li>
                     <a href="realisation.html">Realisations</a>
                    
                </li>
                <li>
                     <a href="galery.html">Galerie</a>
                    
                </li>
                <li>
                    <a href="contact.html">Contact</a>
                </li>
            </ul>
        </div>
        <div class="nav-right-content">
            <a href="devis.html" class="btn btn-radius btn-red mr-2 mb-2">Demander un devis de site web</a>
        </div>
    </div>
</nav>
<!-- navbar area end -->
<!-- breadcrumb area start -->
<div class="breadcrumb-area" style="background-image:url('../../assets/img/page-title-bg.png');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Galerie Altgras</h1>
                    <ul class="page-list">
                        <li><a href="../welcome.html">Accueil</a></li>
                        <li>Galerie</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->

<!-- gallery area start -->
<div class="gallery-area pd-top-120">
    <div class="container">
       <div class="row">
           <div class="col-lg-12">
                <div class="gallery-masonry-wrapper">
                    <div class="gallery-masonry row no-gutters">
                                                <div class="col-md-4 col-sm-6 masonry-item">
                            <a class="single-work-item text-center" data-effect="mfp-zoom-in" href="../../assets/img/gallery/for1.jpg">
                                <span class="thumb">
                                    <img src="../../assets/img/gallery/for1.jpg" alt="gallery">
                                </span>
                                <span class="content-wrap">
                                    <span class="content">
                                        <span class="title">formation1</span>
                                    </span>
                                </span>
                            </a>
                        </div>
                                                <div class="col-md-4 col-sm-6 masonry-item">
                            <a class="single-work-item text-center" data-effect="mfp-zoom-in" href="../../assets/img/gallery/for2.jpg">
                                <span class="thumb">
                                    <img src="../../assets/img/gallery/for2.jpg" alt="gallery">
                                </span>
                                <span class="content-wrap">
                                    <span class="content">
                                        <span class="title">formation2</span>
                                    </span>
                                </span>
                            </a>
                        </div>
                                                <div class="col-md-4 col-sm-6 masonry-item">
                            <a class="single-work-item text-center" data-effect="mfp-zoom-in" href="../../assets/img/gallery/for8.jpg">
                                <span class="thumb">
                                    <img src="../../assets/img/gallery/for8.jpg" alt="gallery">
                                </span>
                                <span class="content-wrap">
                                    <span class="content">
                                        <span class="title">Formation</span>
                                    </span>
                                </span>
                            </a>
                        </div>
                                                <div class="col-md-4 col-sm-6 masonry-item">
                            <a class="single-work-item text-center" data-effect="mfp-zoom-in" href="../../assets/img/gallery/for9.jpg">
                                <span class="thumb">
                                    <img src="../../assets/img/gallery/for9.jpg" alt="gallery">
                                </span>
                                <span class="content-wrap">
                                    <span class="content">
                                        <span class="title">Formation</span>
                                    </span>
                                </span>
                            </a>
                        </div>
                                                <div class="col-md-4 col-sm-6 masonry-item">
                            <a class="single-work-item text-center" data-effect="mfp-zoom-in" href="../../assets/img/gallery/for10.jpg">
                                <span class="thumb">
                                    <img src="../../assets/img/gallery/for10.jpg" alt="gallery">
                                </span>
                                <span class="content-wrap">
                                    <span class="content">
                                        <span class="title">Formation</span>
                                    </span>
                                </span>
                            </a>
                        </div>
                                                <div class="col-md-4 col-sm-6 masonry-item">
                            <a class="single-work-item text-center" data-effect="mfp-zoom-in" href="../../assets/img/gallery/for11.jpg">
                                <span class="thumb">
                                    <img src="../../assets/img/gallery/for11.jpg" alt="gallery">
                                </span>
                                <span class="content-wrap">
                                    <span class="content">
                                        <span class="title">Formation</span>
                                    </span>
                                </span>
                            </a>
                        </div>
                                                <div class="col-md-4 col-sm-6 masonry-item">
                            <a class="single-work-item text-center" data-effect="mfp-zoom-in" href="../../assets/img/gallery/for12.jpg">
                                <span class="thumb">
                                    <img src="../../assets/img/gallery/for12.jpg" alt="gallery">
                                </span>
                                <span class="content-wrap">
                                    <span class="content">
                                        <span class="title">Formation</span>
                                    </span>
                                </span>
                            </a>
                        </div>
                                                <div class="col-md-4 col-sm-6 masonry-item">
                            <a class="single-work-item text-center" data-effect="mfp-zoom-in" href="../../assets/img/gallery/for13.jpg">
                                <span class="thumb">
                                    <img src="../../assets/img/gallery/for13.jpg" alt="gallery">
                                </span>
                                <span class="content-wrap">
                                    <span class="content">
                                        <span class="title">Formation</span>
                                    </span>
                                </span>
                            </a>
                        </div>
                         
                    </div>
                </div>
           </div>
       </div>
    </div>
</div>
<!-- gallery area End -->


<!-- footer area start -->
<footer class="footer-area mg-top-120" style="background-image:url(../../../localhost/altgras/assets/img/bg/footer.html);">
    <div class="footer-top pd-top-120 padding-bottom-40">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-9 col-lg-11">
                    <div class="footer-widget widget text-center">
                        <div class="footer_top_widget">
                            <a href="../welcome.html" class="footer-logo"> 
                                <img src="../../assets/img/logo.png" alt="footer logo">
                            </a>
                            <p>Notre équipe d'assistance sera toujours à votre disposition pour tout besoin</p>
                        </div>
                    </div>
                    <div class="footer-widget widget widget_nav_menu text-center">
                        <ul>
                            <li><a href="../welcome.html">Accueil</a></li>
                            <li><a href="formation.html">Formation</a></li>
                            <li><a href="realisation.html">Realisation</a></li>
                             <li><a href="galery.html">Galerie</a></li>
                            <li><a href="contact.html">Contact</a></li>
                        </ul>
                    </div>
                    <div class="copyright-inner">
                        <div class="row custom-gutters-16">
                            <div class="col-lg-7">
                                <div class="copyright-text">
                                    &copy;Tous droits réservés<a href="https://codingeek.net/" target="_blank"><i class="fa fa-heart"></i><span>altgras.</span></a>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <ul class="social-icon text-right">
                                   
                                    <li>
                                        <a class="facebook" href="https://www.facebook.com/altgrascompany" target="_blank"><i class="fa fa-facebook  "></i></a>
                                    </li>
                                    <li>
                                        <a class="twitter" href="https://twitter.com/altgrascompany" target="_blank"><i class="fa fa-twitter  "></i></a>
                                    </li>
                                    <li>
                                        <a class="linkedin" href="https://www.linkedin.com/altgrascompany" target="_blank"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer area end -->

<!-- back to top area start -->
<div class="back-to-top">
    <span class="back-top"><i class="fa fa-angle-up"></i></span>
</div>
<!-- back to top area end -->


    <!-- jquery -->
    <script src="../../assets/js/jquery-2.2.4.min.js"></script>
    <!-- popper -->
    <script src="../../assets/js/popper.min.js"></script>
    <!-- bootstrap -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <!-- magnific popup -->
    <script src="../../assets/js/jquery.magnific-popup.js"></script>
    <!-- wow -->
    <script src="../../assets/js/wow.min.js"></script>
    <!-- owl carousel -->
    <script src="../../assets/js/owl.carousel.min.js"></script>
    <!-- slick slider -->
    <script src="../../assets/js/slick.js"></script>
    <!-- cssslider slider -->
    <script src="../../assets/js/jquery.cssslider.min.js"></script>
    <!-- waypoint -->
    <script src="../../assets/js/waypoints.min.js"></script>
    <!-- counterup -->
    <script src="../../assets/js/jquery.counterup.min.js"></script>
    <!-- imageloaded -->
    <script src="../../assets/js/imagesloaded.pkgd.min.js"></script>
    <!-- isotope -->
    <script src="../../assets/js/isotope.pkgd.min.js"></script>
    <!-- world map -->
    <script src="../../assets/js/worldmap-libs.js"></script>
    <script src="../../assets/js/worldmap-topojson.js"></script>
    <script src="../../assets/js/mediaelement.min.js"></script>
     <!-- main js -->
    <script src="../../assets/js/main.js"></script>

</body>

<!-- Mirrored from codingeek.net/html/riyaqas/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 17 Feb 2020 03:47:11 GMT -->

<!-- Mirrored from altgras.com/index.php/welcome/galery by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 15 Aug 2020 09:12:13 GMT -->
</html>