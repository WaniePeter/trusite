<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TRU CREATIVE</title>
    
    <!-- animate -->
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="../../assets/css/magnific-popup.css">
    <!-- owl carousel -->
    <link rel="stylesheet" href="../../assets/css/owl.carousel.min.css">
    <!-- icons -->
    <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../assets/css/line-awesome.min.css">
    <link rel="stylesheet" href="../../assets/css/themify-icons.css">
    <link rel="stylesheet" href="../../assets/css/flaticon.css">
    <!-- slick slider -->
    <link rel="stylesheet" href="../../assets/css/slick.css">
    <!-- animated slider -->
    <link rel="stylesheet" href="../../assets/css/animated-slider.css">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- responsive Stylesheet -->
    <link rel="stylesheet" href="../../assets/css/responsive.css">

</head>
<body>

<!-- preloader area start -->
<div class="preloader" id="preloader">
    <div class="preloader-inner">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<!-- preloader area end -->


<!-- navbar area start -->
<nav class="navbar navbar-area navbar-expand-lg nav-style-01">
    <div class="container nav-container">
        <div class="responsive-mobile-menu">
            <div class="logo-wrapper mobile-logo">
                <a href="../welcome.html" class="logo">
                   <img src="../../assets/img/logo.png" alt="logo">
                </a>
            </div>
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#Riyaqas_main_menu" 
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggle-icon">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="Riyaqas_main_menu">
            <div class="logo-wrapper desktop-logo">
                <a href="../welcome.html" class="logo">
                    <img src="../../assets/img/logo.png" alt="logo">
                </a>
            </div>
            <ul class="navbar-nav">
                <li >
                    
                    <a href="../welcome.html">Accueil</a>
                
                </li>
                
                <li>
                    <a href="formation.html">Formations</a>
                </li>
                <li>
                     <a href="realisation.html">Realisations</a>
                    
                </li>
                
                    <li><a href="../team.html">Team</a> </li>
               
                <li>
                    <a href="contact.html">Contact</a>
                </li>
            </ul>
        </div>
        <div class="nav-right-content">
            <a href="devis.html" class="btn btn-radius btn-red mr-2 mb-2">Demander un devis de site web</a>
        </div>
    </div>
</nav>
<!-- navbar area end -->
<!-- breadcrumb area start -->
<div class="breadcrumb-area" style="background-image:url('../../assets/img/page-title-bg.png');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Formation TRU CREATIVE</h1>
                    <!-- <ul class="page-list">
                        <li><a href="../welcome.html">Accueil</a></li>
                        <li>Formation</li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->


<div class="blog-page-area pd-top-120">
    <div class="container">
        <div class="row custom-gutters-60">
            <div class="col-lg-8">
                <div class="section-title style-four">
                    <h2 class="title text-center">Listes des <span>Formations</span> </h2>
                </div>
                <!-- single job list -->
                                <div class="single-job-list media">
                    <img src="../../assets/img/formation/php.jpg" style="width: 200px;height: 100px">
                    <!-- <p>Image: php.jpg</p> -->
                    <div class="media-body">
                        <h6 class="text-center text-danger">PHP</h6>
                        <span>Date: 2021-04-15</span>
                        <p>Lieu: Ligne</p>
                        <p>Durée: 40 H</p>
                        <p>Concevez des sites web modernes avec le langage le plus populaire des langages web le&#8230;</p>
                        <p>Prix: 200 000 FCFA</p>
                    </div>
                    <a href="presentation/1/1.html" class="job-apply-btn btn-blue align-self-center float-right">Presentation</a>
                    <a href="inscription/1.html" class="job-apply-btn btn-blue align-self-center float-right">Inscription</a>
            
                </div>
                <!-- single job list -->
                
                                <div class="single-job-list media">
                    <img src="../../assets/img/formation/flutter.jpg" style="width: 200px;height: 100px">
                    <!-- <p>Image: flutter.jpg</p> -->
                    <div class="media-body">
                        <h6 class="text-center text-danger">FLUTTER</h6>
                        <span>Date: 2020-04-10</span>
                        <p>Lieu: Fablab Kacella</p>
                        <p>Durée: 45 H</p>
                        <p>Apprendre la programmation mobile avec flutter à partir de zéro</p>
                        <p>Prix: 250 000 FCFA</p>
                    </div>
                    <a href="presentation/2/2.html" class="job-apply-btn btn-blue align-self-center float-right">Presentation</a>
                    <a href="inscription/2.html" class="job-apply-btn btn-blue align-self-center float-right">Inscription</a>
                     <!--<button class="job-apply-btn btn-blue align-self-center float-right">Presentation</button>
                    <button class="job-apply-btn btn-blue align-self-center float-right">Inscription</button>-->
                </div>
                <!-- single job list -->
                
                                <div class="single-job-list media">
                    <img src="../../assets/img/formation/python.jpg" style="width: 200px;height: 100px">
                    <!-- <p>Image: python.jpg</p> -->
                    <div class="media-body">
                        <h6 class="text-center text-danger">PYTHON</h6>
                        <span>Date: 2021-04-30</span>
                        <p>Lieu: Maroua</p>
                        <p>Durée: 45 H</p>
                        <p>La formation complète python pour les debutants</p>
                        <p>Prix: 250 000 FCFA</p>
                    </div>
                    <a href="presentation/3/1.html" class="job-apply-btn btn-blue align-self-center float-right">Presentation</a>
                    <a href="inscription/3.html" class="job-apply-btn btn-blue align-self-center float-right">Inscription</a>
                     <!--<button class="job-apply-btn btn-blue align-self-center float-right">Presentation</button>
                    <button class="job-apply-btn btn-blue align-self-center float-right">Inscription</button>-->
                </div>
                <!-- single job list -->
                
                                <div class="single-job-list media">
                    <img src="../../assets/img/formation/java.jpg" style="width: 200px;height: 100px">
                    <!-- <p>Image: java.jpg</p> -->
                    <div class="media-body">
                        <h6 class="text-center text-danger">JAVA</h6>
                        <span>Date: 2021-05-20</span>
                        <p>Lieu: Maroua</p>
                        <p>Durée: 45 H h</p>
                        <p>Devenez développeur Java 
Apprendre le langage facilement avec des exercices pratique</p>
                        <p>Prix: 300 000 FCFA</p>
                    </div>
                    <a href="presentation/4/1.html" class="job-apply-btn btn-blue align-self-center float-right">Presentation</a>
                    <a href="inscription/4.html" class="job-apply-btn btn-blue align-self-center float-right">Inscription</a>
                     <!--<button class="job-apply-btn btn-blue align-self-center float-right">Presentation</button>
                    <button class="job-apply-btn btn-blue align-self-center float-right">Inscription</button>-->
                </div>
                <!-- single job list -->
                
                                <div class="single-job-list media">
                    <img src="../../assets/img/formation/android.jpg" style="width: 200px;height: 100px">
                    <!-- <p>Image: android.jpg</p> -->
                    <div class="media-body">
                        <h6 class="text-center text-danger">ANDROID</h6>
                        <span>Date: 2020-04-15</span>
                        <p>Lieu: Kacella Labs</p>
                        <p>Durée: 50 H</p>
                        <p>Créer une application android avec le langage JAVA</p>
                        <p>Prix: 250 000 FCFA</p>
                    </div>
                    <a href="presentation/5/2.html" class="job-apply-btn btn-blue align-self-center float-right">Presentation</a>
                    <a href="inscription/5.html" class="job-apply-btn btn-blue align-self-center float-right">Inscription</a>
                     <!--<button class="job-apply-btn btn-blue align-self-center float-right">Presentation</button>
                    <button class="job-apply-btn btn-blue align-self-center float-right">Inscription</button>-->
                </div>
                <!-- single job list -->
                
                                <div class="single-job-list media">
                    <img src="../../assets/img/formation/kotlin.jpg" style="width: 200px;height: 100px">
                    <!-- <p>Image: kotlin.jpg</p> -->
                    <div class="media-body">
                        <h6 class="text-center text-danger">KOTLIN</h6>
                        <span>Date: 2021-04-10</span>
                        <p>Lieu: Maroua</p>
                        <p>Durée: 50 H</p>
                        <p>Apprendre le développement Android avec kotlin</p>
                        <p>Prix:  300 000 FCFA</p>
                    </div>
                    <a href="presentation/6/2.html" class="job-apply-btn btn-blue align-self-center float-right">Presentation</a>
                    <a href="inscription/6.html" class="job-apply-btn btn-blue align-self-center float-right">Inscription</a>
                     <!--<button class="job-apply-btn btn-blue align-self-center float-right">Presentation</button>
                    <button class="job-apply-btn btn-blue align-self-center float-right">Inscription</button>-->
                </div>
                <!-- single job list -->
                
                                <div class="single-job-list media">
                    <img src="../../assets/img/formation/js.jpg" style="width: 200px;height: 100px">
                    <!-- <p>Image: js.jpg</p> -->
                    <div class="media-body">
                        <h6 class="text-center text-danger">JAVASCRIPT</h6>
                        <span>Date: 2021-04-30</span>
                        <p>Lieu: Maroua</p>
                        <p>Durée: 45 H</p>
                        <p>Un cours de javascript facile à digérer plein d'exercices</p>
                        <p>Prix: 250 000 FCFA</p>
                    </div>
                    <a href="presentation/7/1.html" class="job-apply-btn btn-blue align-self-center float-right">Presentation</a>
                    <a href="inscription/7.html" class="job-apply-btn btn-blue align-self-center float-right">Inscription</a>
                     <!--<button class="job-apply-btn btn-blue align-self-center float-right">Presentation</button>
                    <button class="job-apply-btn btn-blue align-self-center float-right">Inscription</button>-->
                </div>
                <!-- single job list -->
                
                                <div class="single-job-list media">
                    <img src="../../assets/img/formation/angular.jpg" style="width: 200px;height: 100px">
                    <!-- <p>Image: angular.jpg</p> -->
                    <div class="media-body">
                        <h6 class="text-center text-danger">ANGULAR</h6>
                        <span>Date: 2021-07-24</span>
                        <p>Lieu: Maroua</p>
                        <p>Durée: 35 H</p>
                        <p>Apprenez les concepts fondamentaux du framework Angular</p>
                        <p>Prix: 250 000 FCFA</p>
                    </div>
                    <a href="presentation/8/1.html" class="job-apply-btn btn-blue align-self-center float-right">Presentation</a>
                    <a href="inscription/8.html" class="job-apply-btn btn-blue align-self-center float-right">Inscription</a>
                     <!--<button class="job-apply-btn btn-blue align-self-center float-right">Presentation</button>
                    <button class="job-apply-btn btn-blue align-self-center float-right">Inscription</button>-->
                </div>
                <!-- single job list -->
                
                                <div class="single-job-list media">
                    <img src="../../assets/img/formation/Bootstrap.jpg" style="width: 200px;height: 100px">
                    <!-- <p>Image: Bootstrap.jpg</p> -->
                    <div class="media-body">
                        <h6 class="text-center text-danger">Bootstrap</h6>
                        <span>Date: 2021-06-01</span>
                        <p>Lieu: Kacella Labs</p>
                        <p>Durée: 20 H</p>
                        <p>Apprenez bootstrap le framework css pour rendre votre tâches plus facile et gangner en temps</p>
                        <p>Prix: 100 000 FCFA</p>
                    </div>
                    <a href="presentation/9/1.html" class="job-apply-btn btn-blue align-self-center float-right">Presentation</a>
                    <a href="inscription/9.html" class="job-apply-btn btn-blue align-self-center float-right">Inscription</a>
                     <!--<button class="job-apply-btn btn-blue align-self-center float-right">Presentation</button>
                    <button class="job-apply-btn btn-blue align-self-center float-right">Inscription</button>-->
                </div>
                <!-- single job list -->
                
                                <!--
                <div class="riyaqas-pagination margin-top-100">
                    <ul>
                        <li><a class="prev page-numbers" href="#"><i class="ti-angle-left"></i></a></li>
                        <li><span class="page-numbers">1</span></li>
                        <li><span class="page-numbers current">2</span></li>
                        <li><a class="page-numbers" href="#">3</a></li>
                        <li><a class="page-numbers" href="#">4</a></li>
                        <li><a class="next page-numbers" href="#"><i class="ti-angle-right"></i></a></li>
                    </ul>                          
                </div>
                -->
            </div>
            <div class="col-lg-4">
                <aside class="sidebar-area">
                    <div class="widget widget_search">
                        <form class="search-form">
                            <div class="form-group">
                                <input type="text" placeholder="Search">
                            </div>
                            <button class="submit-btn" type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    
                    <div class="widget widget_categories">
                        <h2 class="widget-title">Category</h2>
                        <ul>
                            <li><a href="formation/index.html">Toutes les categories</a></li>
                        
                            <li><a href="formation/1.html">Web</a></li>
                        
                            <li><a href="formation/2.html">Mobile</a></li>
                                                    
                        </ul>
                    </div>
                    <!--
                    <div class="widget widget_tag_cloud">
                        <h3 class="widget-title">Popular Tag</h3>
                        <div class="tagcloud">
                            <a href="#">Software</a>
                            <a href="#">Agency</a>
                            <a href="#">Creative</a>
                            <a href="#">Startup</a>
                            <a href="#">Marketing</a>
                            <a href="#">Digital</a>
                        </div>
                    </div>
                    -->
                    <div class="widget widget-newslatter">
                        <h3 class="widget-title">Newslatter</h3>
                        <div class="newsletter-subcribe">
                            <form action="https://altgras.com/index.php/welcome/newsletter" class="subcribe-form" id="news-subcribeform" method="post" accept-charset="utf-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Votre E-mail..." name="email" required="">
                                    <input type="hidden" name="form1" value="formation" class="form-control" >
                                    <button type="submit" class="btn-blue subcribe-submit">Envoyer</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</div>






<!-- footer area start -->
<footer class="footer-area mg-top-120" style="background-image:url(../../../localhost/altgras/assets/img/bg/footer.html);">
    <div class="footer-top pd-top-120 padding-bottom-40">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-9 col-lg-11">
                    <div class="footer-widget widget text-center">
                        <div class="footer_top_widget">
                            <a href="../welcome.html" class="footer-logo"> 
                                <img src="../../assets/img/logo.png" alt="footer logo">
                            </a>
                            <p>Notre équipe d'assistance sera toujours à votre disposition pour tout besoin</p>
                        </div>
                    </div>
                    <div class="footer-widget widget widget_nav_menu text-center">
                        <ul>
                            <li><a href="../welcome.html">Accueil</a></li>
                            <li><a href="formation.html">Formation</a></li>
                            <li><a href="realisation.html">Realisation</a></li>
                            <li><a href="../team.html">Team</a> </li>
                            <li><a href="contact.html">Contact</a></li>
                        </ul>
                    </div>
                    <div class="copyright-inner">
                        <div class="row custom-gutters-16">
                            <div class="col-lg-7">
                                <div class="copyright-text">
                                    &copy;Tous droits réservés<a href="https://codingeek.net/" target="_blank"><i class="fa fa-heart"></i><span>TRU.</span></a>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <ul class="social-icon text-right">
                                   
                                    <li>
                                        <a class="facebook" href="" target="_blank"><i class="fa fa-facebook  "></i></a>
                                    </li>
                                    <li>
                                        <a class="twitter" href="" target="_blank"><i class="fa fa-twitter  "></i></a>
                                    </li>
                                    <li>
                                        <a class="linkedin" href="" target="_blank"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer area end -->

<!-- back to top area start -->
<div class="back-to-top">
    <span class="back-top"><i class="fa fa-angle-up"></i></span>
</div>
<!-- back to top area end -->


    <!-- jquery -->
    <script src="../../assets/js/jquery-2.2.4.min.js"></script>
    <!-- popper -->
    <script src="../../assets/js/popper.min.js"></script>
    <!-- bootstrap -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <!-- magnific popup -->
    <script src="../../assets/js/jquery.magnific-popup.js"></script>
    <!-- wow -->
    <script src="../../assets/js/wow.min.js"></script>
    <!-- owl carousel -->
    <script src="../../assets/js/owl.carousel.min.js"></script>
    <!-- slick slider -->
    <script src="../../assets/js/slick.js"></script>
    <!-- cssslider slider -->
    <script src="../../assets/js/jquery.cssslider.min.js"></script>
    <!-- waypoint -->
    <script src="../../assets/js/waypoints.min.js"></script>
    <!-- counterup -->
    <script src="../../assets/js/jquery.counterup.min.js"></script>
    <!-- imageloaded -->
    <script src="../../assets/js/imagesloaded.pkgd.min.js"></script>
    <!-- isotope -->
    <script src="../../assets/js/isotope.pkgd.min.js"></script>
    <!-- world map -->
    <script src="../../assets/js/worldmap-libs.js"></script>
    <script src="../../assets/js/worldmap-topojson.js"></script>
    <script src="../../assets/js/mediaelement.min.js"></script>
     <!-- main js -->
    <script src="../../assets/js/main.js"></script>

</body>


</html>